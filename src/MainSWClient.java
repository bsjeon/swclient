import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.MalformedURLException;
import java.util.StringTokenizer;


public class MainSWClient {
	public static void main(String[] args)
	{
		/*
		 * Create the POST request
		 */
		try {
			/**
			 * Read info from file to get DongCode & DongName.
			 */
			FileOutputStream fos = new FileOutputStream("HandsomeSW.txt"); 
			OutputStreamWriter osw = new OutputStreamWriter(fos,"EUC-KR"); 
			BufferedWriter bw = new BufferedWriter(osw);	
			
			for (int year = 2006; year <= 2013; ++year) {
				for (int period = 1; period <= 4; ++period) {
					FileInputStream fis = new FileInputStream(new File("DongCode.csv")); 
					InputStreamReader isr = new InputStreamReader(fis,"EUC-KR"); 
					BufferedReader br = new BufferedReader(isr);	

					//Debug
					int maxNumForCities = 30000; // MAX : 20551 (assume 30000 for spare)
					int numForCities= 0;

					while(true){
						// 0 : DongCode, 1,2,3,4 : Si, Gu, Dong, Li,  5: Useless number
						String[] dongNames = new String[6];
						int arrayLen = 0;
						
						String str = br.readLine();
						// END COND
						if(str==null) break;
						StringTokenizer strToken = new StringTokenizer(str, ","); 
						while (strToken.hasMoreTokens()) {
							String tmpStr = strToken.nextToken();
							dongNames[arrayLen++] = tmpStr;
						}

						// make result Line
						String resultLine = "";
						for (int i=1; i<arrayLen-1; ++i) {
							resultLine += dongNames[i];
							resultLine += ",";
						}
						// add comma for blank
						for (int i=arrayLen; i<6; ++i) {
							resultLine += ",";
						}
						
//						System.out.println(resultLine);
						
						SWClient swClient = new SWClient();
						swClient.setYear(year);
						swClient.setPeriod(period);
						swClient.sendRequest(dongNames[0], resultLine);
						// delete swClient
						/**
						 * Write Data to file
						 */
						bw.write(swClient.getResult());
						swClient = null;
//						System.out.println(""+numForCities + dongNames[0]);
						++numForCities;
						if (numForCities>maxNumForCities) break;
					}
					br.close();
				}
			}
			bw.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
