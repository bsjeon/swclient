import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class SWClient {
	private int year;
	private int period;
	private String dongCode;
	private String result = "";
	
	public String getResult()
	{
		return this.result;
	}
	
	public void setYear(int year)
	{
		this.year = year;
	}
	public void setPeriod (int period)
	{
		this.period = period;
	}
	
	public void sendRequest(String dongCodeStr, String firstOfResultLine) throws MalformedURLException, IOException
	{		
		dongCode = dongCodeStr;
		String urlParameters = "dongCode="
				+ dongCodeStr + "&danjiCode=ALL&srhYear=" + year
				+ "&srhPeriod=" + period;
		String request = "http://rt.molit.go.kr/rtApt.do?cmd=getTradeAptLocal";
		URL url = new URL(request); 
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();           

		// if setDoOutput as true, I can include body(when POST method)
		// setDoInput doesn't appear to do sth.
		connection.setDoOutput(true);
		connection.setDoInput(true);

		// if false, I can use session
		connection.setInstanceFollowRedirects(false);
		connection.setRequestMethod("POST"); 
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
		connection.setRequestProperty("charset", "utf-8");
		connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length));
		connection.setUseCaches (false);

		DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
//		System.out.println("send POST Success");

		/**
		 *  Get Request
		 */
		InputStream in = null ;
		String inputStr = null;
		/// 받아온 데이터를 쓰기위한 스트림
		ByteArrayOutputStream bos = new ByteArrayOutputStream( ) ;

		/// 리퀘스트 데이터를 저장할 버퍼
		byte[] buf = new byte[2048];
		int k = 0 ; /// 읽은 라인수
		/// == 시간 체크용 == 서버에 따라 리퀘스트 오는 시간이 매우 오래걸림
		long ti = System.currentTimeMillis( ) ;
		/// 인풋스트림 생성
		in = connection.getInputStream( ) ;       

		/// == 시간 체크용 == inputstream얻는 요기서 시간 10초이상 넘어가면 큰일남
		/// 갤럭시 S에서 어떤앱은 WebView라던가 Http통신에서 15초인가 넘어가면 세션 끊기는
		/// 원인을 알 수 없는 경우도 있었음 다른기기 다 잘되는데 오로지 갤럭시 S만!!! 그랬음 참고 바람요
//		System.out.println( "recTime : " + ( System.currentTimeMillis( ) - ti ) ) ;
		/// 루프를 돌면서 리퀘스트로 받은내용을 저장한다.
		while( true )
		{
			int readlen = in.read( buf ) ;
			if( readlen < 1 )
				break ;
			k += readlen ;
			bos.write( buf, 0, readlen ) ;
		}
		/// 리퀘스트 받은 내용을 UTF-8로 변경해서 문자열로 저장
		inputStr = new String( bos.toByteArray( ), "UTF-8" ) ;
		/*
		File fl = new File( "/sdcard/rec.txt" ) ;
		FileOutputStream fos = new FileOutputStream( fl ) ;
		fos.write( bos.toByteArray( ) ) ;
		/**/
//		System.out.print(inputStr);
		
		printResponseAfterJSONParsing(inputStr, firstOfResultLine);
		connection.disconnect();
	}
	
	private void printResponseAfterJSONParsing(String inputStr, String firstOfResultLine)
	{
		try {
			/**
			 * JSON PARSING
			 */
			JSONParser jparser = new JSONParser();
			JSONObject jobj = (JSONObject) jparser.parse(inputStr);
			
			JSONArray jArray = (JSONArray) jobj.get("danjiList");
			Iterator<JSONObject> itr = jArray.iterator();
			
			// Make map for aligning DATA, get APT_NAME
			
			HashMap<String, String> aptMap = new HashMap<String, String>();
			
			// if NULL, halt.
			if (!itr.hasNext()) {
				return;
			}
			
			while (itr.hasNext()) {
				JSONObject tmpObj = itr.next();
				aptMap.put((String)tmpObj.get("APT_CODE"), (String)tmpObj.get("APT_NAME"));
			}
			
			// get MONTH, SUM_AMT(Price), DAYS, FLOOR, AREA(Pyung)
			JSONArray jDetailArray = (JSONArray) jobj.get("detailList");
			itr = jDetailArray.iterator();
			while (itr.hasNext()) {
				JSONObject tmpObj = itr.next();
				String aptCode = (String)tmpObj.get("APT_CODE");
				String aptName = aptMap.get(aptCode);
				String rawDealingAmount = (String)tmpObj.get("SUM_AMT"); 
				String dealingAmount = rawDealingAmount.replace(",", "");
				String month = (String)tmpObj.get("MONTH");
				String days = (String)tmpObj.get("DAYS");
				String floor = (String)tmpObj.get("FLOOR");
				String area = (String)tmpObj.get("AREA");
				
				result += dongCode+","+firstOfResultLine + aptName + ","+ floor + ","+ area
						 + ","+ dealingAmount +
						 "," + year + ","+ month + ","+ days+"\n"; 
			}
		} catch(org.json.simple.parser.ParseException e) {
			e.printStackTrace();
		}
	}
}
